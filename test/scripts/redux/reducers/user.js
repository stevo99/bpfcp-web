import expect from 'expect'
import userReducer from '../../../../src/scripts/redux/reducers/user.js'

describe('when user reducer', () => {
  describe('is initialised', () => {
    const initialState = userReducer(undefined, {})
    it('token is null', () => {
      expect(initialState).toContain({ token: null })
    })
    it('loggedIn is false', () => {
      expect(initialState).toContain({ loggedIn: false })
    })
  })

  describe('receives USER_LOGIN_ERROR action', () => {
    const error = 'error!'
    const state = userReducer(undefined, { type: 'USER_LOGIN_ERROR', payload: error })

    it('errorText set from action payload', () => {
      expect(state).toContain({ loginError: error })
    })
    it('loading is false', () => {
      expect(state).toContain({ loading: false })
    })
  })

  describe('receives USER_LOGIN_REQUEST action', () => {
    const state = userReducer({ loginError: 'oldError' }, { type: 'USER_LOGIN_REQUEST' })

    it('loginError is cleared', () => {
      expect(state).toContain({ loginError: null })
    })
    it('loading is true', () => {
      expect(state).toContain({ loading: true })
    })
  })

  describe('receives USER_LOGIN_SUCCESS action', () => {
    const token = 'token'
    const state = userReducer({ loading: true }, { type: 'USER_LOGIN_SUCCESS', payload: { token: token } })

    it('token set from action payload', () => {
      expect(state).toContain({ token: token })
    })
    it('loading is false', () => {
      expect(state).toContain({ loading: false })
    })
    it('loggedIn is true', () => {
      expect(state).toContain({ loggedIn: true })
    })
  })

  describe('receives USER_ACCOUNT_NUMBER_UPDATED action', () => {
    const accountNumber = 'accountNumber'
    const state = userReducer(undefined, { type: 'USER_ACCOUNT_NUMBER_UPDATED', payload: accountNumber })

    it('accountNumber set from action payload', () => {
      expect(state).toContain({ accountNumber: accountNumber })
    })
  })
})
