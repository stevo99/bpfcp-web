require 'active_support/core_ext/string/inflections'

class App

  def login
    Login.new
  end

  def my_account
    MyAccount.new
  end

end