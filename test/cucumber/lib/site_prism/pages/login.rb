class Login < SitePrism::Page

  set_url '/login'
  set_url_matcher %r{/login}

  element :agreement_number, '.account-number'
  element :date_of_birth, '.dob'
  element :postcode, '.postcode'
  element :login, '.login'


  def login_user(user_details)
    agreement_number.set(user_details[:agreement_no])
    postcode.set(user_details[:postcode])
    date_of_birth.set(user_details[:dob])
    login.click
  end



end