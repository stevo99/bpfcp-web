class ConfigHelper

  def self.method_missing(method_sym, *args, &block)
    meth_name = method_sym.to_s
    if meth_name =~ /^load_(.*)$/
      begin
        yml = YAML.load_file File.dirname(__FILE__) + "/../config/#{$1}.yml"
        yml
      rescue
        raise "There is no config for #{$1} in the config directory"
      end
    else
      super
    end
  end


end