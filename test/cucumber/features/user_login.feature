Feature: User Login

  As a Barclays Partner Finance customer
  I want to be able to login to the site to view my transactions

  Scenario: Successful login
    Given I am on the login page
    When I login with valid credentials
    Then I will be logged into my account

  @wip
  Scenario: Unsuccessful login
    Given I am on the login page
    When I login with invalid credentials
    Then I will not be logged into my account

  @wip
  Scenario Outline: Login form validations - Agreement Number
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the agreement field
    Examples:
      | agreement | dob        | postcode |
      |           | 01/01/1950 | EC11AA   |
      | 1234567   | 01/01/1950 | EC11AA   |
      | 12345678  | 01/01/1950 | EC11AA   |
      | letters   | 01/01/1950 | EC11AA   |
      | 1234 567  | 01/01/1950 | EC11AA   |

  @wip
  Scenario Outline: Login form validations - Date of Birth
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the dob field
    Examples:
      | agreement        | dob        | postcode |
      | 1234567890123456 |            | EC11AA   |
      | 1234567890123456 | 01011950   | EC11AA   |
      | 1234567890123456 | 01/01/50   | EC11AA   |
      | 1234567890123456 | 01-01-1950 | EC11AA   |

  @wip
  Scenario Outline: Login form validations - Postcode
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the postcode field
    Examples:
      | agreement        | dob        | postcode |
      | 1234567890123456 | 01/01/1950 |          |
      | 1234567890123456 | 01/01/1950 | E1AA     |
      | 1234567890123456 | 01/01/1950 | EC1 1AA  |
      | 1234567890123456 | 01/01/1950 | ECC11AA  |
      | 1234567890123456 | 01/01/1950 | EC111A   |
      | 1234567890123456 | 01/01/1950 | EC11A    |





