Given(/^I am on the login page$/) do
  @app.login.load
end

When(/^I login with (.*) credentials$/) do |login_type|
  customer = ConfigHelper.load_customer
  binding.pry
  case login_type
    when 'valid'
      @app.login.login_user({agreement_no: customer[:account_number], dob: customer[:dob], postcode: customer[:postcode]})
    when 'invalid'
      @app.login.login_user({agreement_no: '00000000', dob: '01/01/1970', postcode: 'EC1A 1AA'})
    else
      raise 'expected either valid or invalid as options'
  end

end

When(/^I login with "([^"]*)" "([^"]*)" "([^"]*)"$/) do |agreement, dob, postcode|
  user_hash = {agreement_no: agreement, dob: dob, postcode: postcode}
  @app.login.login_user(user_hash)
end

Then(/^I (.*) be logged into my account$/) do |status|

  case status
    when 'will'
      expect(@app.my_account.customer_name.text).to eq('Hello Mr Customer')
    when 'will not'
      expect(@app.login).to be_displayed
  #     expect(error message...)
  end
end
