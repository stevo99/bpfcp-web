/* eslint-disable*/
import _ from 'lodash'
import defaults from './defaults'
import productionConfig from './production'
import developmentConfig from './development'
import localConfig from './local'

const environment = process.env.NODE_ENV

const getEnvironment = () => {
  switch (environment) {
    case 'production':
      return(productionConfig)
    case 'development':
      return(developmentConfig)
    case 'local':
      return(localConfig)
    default:
      return(developmentConfig)
  }
}

const config = _.merge({...defaults}, getEnvironment())
export default config
