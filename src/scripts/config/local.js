import development from './development'

export default { ...development, backendUrl: 'http://localhost:5000' }
