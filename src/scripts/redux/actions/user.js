import request from 'superagent-es6-promise'
import config from '../../config'
export default {
  login() {
    return (dispatch, getState) => {
      dispatch({ type: 'USER_LOGIN_REQUEST' })
      request.post(`${config.backendUrl}/login`)
        .send({ accountNumber: getState().user.accountNumber })
        .then((result) => dispatch({ type: 'USER_LOGIN_SUCCESS', payload: result.body }))
        .catch((error) => dispatch({ type: 'USER_LOGIN_ERROR', payload: error.body.message }))
    }
  },
  updateAccountNumber(accountNumber) {
    return { type: 'USER_ACCOUNT_NUMBER_UPDATED', payload: accountNumber }
  }
}
