
const initialState = {
  loading: false,
  loggedIn: false,
  accountNumber: null,
  loginError: null,
  token: null
}

export default function user(state = initialState, action) {
  switch (action.type) {
    case 'USER_LOGIN_REQUEST':
      return { ...state, loading: true, loginError: null }
    case 'USER_LOGIN_SUCCESS':
      return { ...state, loading: false, token: action.payload.token, loggedIn: true }
    case 'USER_LOGIN_ERROR':
      return { ...state, loading: false, loginError: action.payload }
    case 'USER_ACCOUNT_NUMBER_UPDATED':
      return { ...state, accountNumber: action.payload }
    default:
      return state
  }
}
