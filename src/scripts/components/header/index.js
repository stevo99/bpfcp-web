import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import NavigationHeader from './navigationHeader'

class Header extends Component {
  logoutButton() {
    return this.props.loggedIn ? <Link className="button" to="/logout">Log out</Link> : null
  }

  navigationHeader() {
    return this.props.loggedIn ? <NavigationHeader /> : null
  }

  render() {
    return (
      <div>
        <div className="header header-outside">
          <div className="header-items">
            <h5 className="header-title">Partner Finance</h5>
            {this.logoutButton()}
          </div>
        </div>
        {this.navigationHeader()}
      </div>
    )
  }
}

export default connect((state) => state.user)(Header)
