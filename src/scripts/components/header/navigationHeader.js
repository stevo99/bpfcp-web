import React from 'react'
import { Link } from 'react-router'

export default () => (
  <div className="content">
    <h2>Barclays</h2>
    <div className="navigator-header">
      <Link to="/account-summary"><h5 className="navigator-item">Account Summary</h5></Link>
      <Link to="/make-payment"><h5 className="navigator-item">Make a payment</h5></Link>
      <Link to="/settlement-quote"><h5 className="navigator-item">Settlement Quote</h5></Link>
    </div>
  </div>
)
