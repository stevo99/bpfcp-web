import React, { Component } from 'react'
import { connect } from 'react-redux'
import userActions from '../redux/actions/user'
import Header from './header'
@connect((state) => state, userActions)
export default class App extends Component {
  static propTypes = {
    children: React.PropTypes.any.isRequired
  }

  render() {
    return (
      <div id="main">
        <Header />
        <div className="content">
          {this.props.children}
        </div>
      </div>
    )
  }
}
