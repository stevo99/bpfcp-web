import React from 'react'
import _ from 'lodash'
import { months } from './common'
import Chart from './chart'
export default () => (
  <div>
    <h2 className="customer-name">Hello Mr Customer</h2>
    <p>You last logged in on Tue 22 March 2016 at 7:47pm</p>
    <Chart />
    <h5 className="landing-heading">Payment Schedule</h5>
    <table>
      <tbody>
        <tr><td>Next payment amount</td><td>£125.63</td></tr>
        <tr><td>Next payment date</td><td>12 April 2016</td></tr>
        <tr><td>Payment method</td><td>Direct debit<br />Account ending 1234</td></tr>
      </tbody>
    </table>
    <h5 className="landing-heading">Payment History</h5>
    <table>
      <tbody>
        <tr><td />{_.map(months, (m, i) => <td key={i}>{m}</td>)}</tr>
        <tr><td>2015</td>{_.map(months, (m, i) => <td className="tick" key={i}>{'\u2713'}</td>)}</tr>
        <tr><td>2016</td>{_.map(months, (m, i) => <td className="tick" key={i}>{i < 3 ? '\u2713' : null}</td>)}</tr>
      </tbody>
    </table>
  </div>
)
