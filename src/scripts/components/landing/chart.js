import React from 'react'
import { Line as LineChart } from 'react-chartjs'
import _ from 'lodash'
import { months } from './common'

const chartData = {
  labels: months,
  datasets: [
    {
      label: 'Loan Repayments',
      fillColor: '#5CAFCC',
      strokeColor: '#88E654',
      pointColor: '#88E654',
      pointStrokeColor: '#88E654',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(220,220,220,1)',
      data: _.range(1000, 300, -60)
    }
  ]
}

const chartOptions = {
  animation: true,
  animationSteps: 60,
  responsive: true,
  scaleShowGridLines: false,
  scaleBeginAtZero: true,
  scaleLabel: '£<%=value%>',
  scaleOverride: true,
  scaleSteps: 5,
  scaleStepWidth: 200,
  scaleStartValue: 0,
  datasetStrokeWidth: 10,
  pointDot: false,
  scaleFontFamily: 'BalsamiqSans'
}

export default () => (
  <div className="landing-chart">
    <LineChart height="75" data={chartData} options={chartOptions} />
  </div>
)
