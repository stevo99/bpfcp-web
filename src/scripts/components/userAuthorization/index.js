import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

@connect((state) => state.user)
export default class UserAuthorization extends Component {
  static propTypes = {
    token: React.PropTypes.string,
    children: React.PropTypes.any.isRequired
  }
  componentWillMount() {
    if (!this.props.token) {
      browserHistory.push('/login')
    }
  }

  render() {
    return (this.props.children)
  }
}
