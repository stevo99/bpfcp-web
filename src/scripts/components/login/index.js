import React, { input, Component } from 'react'
import Form from '../core/form'
import { connect } from 'react-redux'
import userActions from '../../redux/actions/user'
import { browserHistory } from 'react-router'

class Login extends Component {
  componentWillReceiveProps(newProps) {
    if (newProps.loggedIn) {
      browserHistory.push('/')
    }
  }

  render() {
    return (
      <Form onSubmit={() => this.props.login()}>
        <input
          className="login-input account-number"
          placeholder="Account number"
          value={this.props.accountNumber}
          onChange={(e) => this.props.updateAccountNumber(e.target.value)}
        />
        <input
          className="login-input postcode"
          placeholder="Postcode"
        />
        <input
          className="login-input dob"
          placeholder="Date of Birth"
        />
        <input type="submit" value="Login" className="login"></input>
        <p>{this.props.loginError}</p>
      </Form>
    )
  }
}

export default connect((state) => state.user, userActions)(Login)
