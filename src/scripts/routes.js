import App from './components/app'
import Landing from './components/landing'
import UserAuthorization from './components/userAuthorization/'
import _ from 'lodash'
import React from 'react'
import Login from './components/login'
import MakePayment from './components/makePayment'
import SettlementQuote from './components/settlementQuote'

const unauthedRoutes = ['login']
const childRoutes = [
  { path: 'login', component: Login },
  { path: 'make-payment', component: MakePayment },
  { path: 'account-summary', component: Landing },
  { path: 'settlement-quote', component: SettlementQuote },

]

function authedComponent(Component) {
  return (props) => (
    <UserAuthorization history={props.history}>
      <Component {...props} />
    </UserAuthorization>
  )
}

const authRoute = (route) => {
  if (_.includes(unauthedRoutes, route.path)) {
    return (route)
  }
  return ({ ...route, component: authedComponent(route.component) })
}

const authedRoutes = _.map(childRoutes, authRoute)

export default {
  path: '/',
  component: App,
  indexRoute: { component: authedComponent(Landing) },
  childRoutes: authedRoutes
}
