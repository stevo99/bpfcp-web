var express = require('express');
var app = express();
var fallback = require('express-history-api-fallback')

app.use(express.static('build'));
app.use(fallback('/', { root: 'build' }))

app.listen(8080, function () {
  console.log('Express listening on port 8080');
});
