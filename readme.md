#BPF Customer Portal - Web

## Commands

Start with `npm install`

Then you can do:

* `npm run build` to generate code into `build` folder
* `npm run dev` to run a watching dev server at [http://localhost:8080](http://localhost:8080)
* `NODE_ENV=local npm run dev` to connect it to your local node server (rather than heroku dev one)
* `npm run test`
* `npm run test:watch`

## Style guide

Take a look at [Airbnb's](https://github.com/airbnb/javascript)
